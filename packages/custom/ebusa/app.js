'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Ebusa = new Module('ebusa');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Ebusa.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Ebusa.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  Ebusa.menus.add({
    title: 'ebusa example page',
    link: 'ebusa example page',
    roles: ['authenticated'],
    menu: 'main'
  });
  
  Ebusa.aggregateAsset('css', 'ebusa.css');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Ebusa.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Ebusa.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Ebusa.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Ebusa;
});
