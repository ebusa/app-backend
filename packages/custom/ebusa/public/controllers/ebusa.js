'use strict';

/* jshint -W098 */
angular.module('mean.ebusa').controller('EbusaController', ['$scope', 'Global', 'Ebusa',
  function($scope, Global, Ebusa) {
    $scope.global = Global;
    $scope.package = {
      name: 'ebusa'
    };
  }
]);
