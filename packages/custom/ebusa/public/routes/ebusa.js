'use strict';

angular.module('mean.ebusa').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('ebusa example page', {
      url: '/ebusa/example',
      templateUrl: 'ebusa/views/index.html'
    });
  }
]);
