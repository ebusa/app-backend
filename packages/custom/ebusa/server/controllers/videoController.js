'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Video = mongoose.model('Video');
var _ = require('lodash');
var grid = require('gridfs-stream');
grid.mongo = mongoose.mongo;
var fs = require('fs');
var gm = require('gm');
var conn = mongoose.connection;
var gfs = grid(conn.db);

var logModule = require('../services/logModule')('VideoController');
var logger = logModule.getLogger();

module.exports = function (ebusa) {

    return {
        /**
         * Find video by id
         */
        video: function (req, res, next, id) {
            try {
                Video.load(id, function (err, video) {
                    if (err || !video) {
                        logger.error("Failed to load video:  " + err);
                        res.status(500, {
                            error: "Failed to load video: " + err
                        });
                    }
                    req.video = video;
                    logger.info("Loaded video with id: " + video.id);
                    next();
                });
            } catch (error) {
                logger.error("Failed to load video: " + err);
                res.status(500, {
                    error: "Failed to load video: " + err
                });
            }
        },
        /**
         * Create an video
         */
        create: function (req, res, next) {
            try {
                var video = new Video(req.body);
                video.creator = req.user;
                video.save(function (err) {
                    if (err) {
                        logger.error("Could not save Vide:o " + err);
                        res.status(500, {
                            error: "Could not save Video: " + err
                        });
                    }
                    req.video = video;
                    logger.info("Created new video with id: " + video.id);
                    next();
                });
            } catch (error) {
                logger.error("Could not create Video: " + error);
                res.status(500, {
                    error: "Could not create Video: " + error
                });
            }
        },

        /**
         * Show a video
         */
        show: function (req, res) {
            try {
                res.json(req.video);
            } catch (error) {
                logger.error("Could not retrieve Video: " + error);
                res.status(500, {
                    error: "Could not retrieve Video: " + error
                });
            }
        },
        destroy: function (req, res) {
            try {
                var video = req.video;

                video.remove(function (err) {
                    if (err) {
                        return res.status(500).json({
                            error: "Could not delete Video: " + error
                        });
                        logger.error("Could not delete Video: " + error);
                    }
                    logger.info("Deleted video with id: " + video._id);
                    res.json(video);
                });

            } catch (error) {
                logger.error("Could not delete Video: " + error);
                res.status(500, {
                    error: "Could not delete Video: " + error
                });
            }
        },
        motherVideos: function (req, res) {
            try {
                var id = req.params.motherId;

                var creatorId = {
                    creator: {
                        _id: id
                    }
                };

                var size = parseInt(req.query.size) || 10;
                var page = (parseInt(req.query.page) * size) || 0;
                Video.find(creatorId)
                    .skip(page)
                    .limit(size)
                    .sort('-createdAt')
                    .populate('creator', 'name username')
                    .exec(function (err, videos) {
                        if (err) {
                            logger.error("Could not retrieve videos " + err);
                            res.status(500, {
                                error: "Could not retrieve videos " + err
                            });
                        }
                        logger.info("Obtained all videos for mother with id: " + id);
                        res.json(videos);
                    });
            } catch (error) {
                logger.error("Could not delete Video: " + error);
                res.status(500, {
                    error: "Could not delete Video: " + error
                });
            }
        },
        /**
         * Attach file to video
         */
        addVideoFile: function (req, res) {
            var video, videoFile, rStream, wStream;

            try {
                videoFile = req.files.file;
                wStream = gfs.createWriteStream({
                    filename: videoFile.name,
                    content_type: videoFile.type,
                    metadata: {
                        video: req.video._id,
                        creator: req.user._id
                    }
                });
                rStream = gm(videoFile.path).stream().pipe(wStream);
                rStream.on('close', function (file) {
                    video = req.video;
                    video.videoFile = {
                        id: file._id,
                        uploaded: Date.now()
                    };
                    video.save(function (err) {
                        if (err) {
                            logger.error("Could not save video: " + err);
                            res.status(500).send({
                                error: "Could not save video: " + err
                            });
                        }
                        logger.info("Attached new file to video: " + video.id);
                        res.json(file);
                    });
                });
                rStream.on('error', function (error) {
                    logger.error("Could not attach file to video: " + error);
                    res.status(500).send({
                        error: "Could not attach file to video: " + error
                    });
                });
            } catch (error) {
                logger.error("Could not attach file to video: " + error);
                res.status(500).send({
                    message: "Could not attach file to video: " + error
                });
            }

        },

        /**
         * Get video file by its Id
         */
        videoFile: function (req, res) {
            try {
                var id = req.params.videoFileId;
                var ObjectID = mongoose.mongo.ObjectID;
                gfs.createReadStream({
                    _id: new ObjectID(id)
                }).pipe(res);
            } catch (error) {
                logger.error("Could not retrieve video file " + error);
                res.status(500).send({
                    error: "Could not save image " + error
                });
            }
        },

        /**
         * Get of logged in user
         */
        mine: function (req, res) {
            try {
                var creatorId = {
                    creator: {
                        _id: req.user._id
                    }
                };
                Video.find(creatorId)
                    .sort('-createdAt')
                    .populate('creator', 'name username')
                    .exec(function (err, videos) {
                        if (err) {
                            logger.error("Could not retrieve videos of current user " + err);
                            res.status(500, {
                                error: "Could not retrieve videos of current user " + err
                            });
                        }
                        logger.info("Obtained all videos of current user");
                        res.json(videos);
                    });
            } catch (error) {
                logger.error("Could not retrieve videos of current user " + error);
                res.status(500, {
                    error: "Could not retrieve videos of current user " + error
                });
            }
        },

        /**
         * Get all videos
         */
        all: function (req, res) {
            try {
                var size = parseInt(req.query.size) || 10;
                var page = (parseInt(req.query.page) * size) || 0;
                Video.find({})
                    .skip(page)
                    .limit(size)
                    .sort('-createdAt')
                    .populate('creator', 'name username')
                    .exec(function (err, videos) {
                        if (err) {
                            logger.error("Could not retrieve videos " + err);
                            res.status(500, {
                                error: "Could not retrieve videos " + err
                            });
                        }
                        logger.info("Obtained all videos from the database");
                        res.json(videos);
                    });
            } catch (error) {
                logger.error("Could not retrieve videos " + error);
                res.status(500, {
                    error: "Could not retrieve videos " + error
                });
            }
        },

        /**
         * Get all videos between given dates
         */
        getAllBetweenGivenDates: function (req, res) {
            try {
                var size = parseInt(req.query.size) || 10;
                var page = (parseInt(req.query.page) * size) || 0;

                var startDate = new Date(req.query.startdate);
                var enddate = new Date(req.query.enddate);

                enddate.setDate(enddate.getDate() + 1);

                Video.find({
                        'createdAt': {
                            '$gte': startDate,
                            '$lt': enddate
                        }
                    })
                    .skip(page)
                    .limit(size)
                    .sort('-createdAt')
                    .populate('creator', 'name username')
                    .exec(function (err, videos) {
                        if (err) {
                            logger.error("Could not retrieve videos " + err);
                            res.status(500, {
                                error: "Could not retrieve videos " + err
                            });
                        }
                        logger.info("Obtained all videos between given dates");
                        res.json(videos);
                    });
            } catch (error) {
                logger.error("Could not retrieve videos " + error);
                res.status(500, {
                    error: "Could not retrieve videos " + error
                });
            }
        }
    };
};