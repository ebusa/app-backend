'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


/**
 * Video Schema
 */
var VideoSchema = new Schema({
  createdAt: {
    type: Date,
    default: Date.now
  },
  information: {
    type: String
  },
  creator: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  videoFile: {
    id: Schema.Types.ObjectId,
    uploaded: {
      type: Date,
      "default": Date.now
    }
  }
});

/**
 * Statics
 */
VideoSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).populate('creator', 'name username').exec(cb);
};

mongoose.model('Video', VideoSchema);
