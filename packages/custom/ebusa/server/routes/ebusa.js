'use strict';

var logModule = require('../services/logModule')('ebusa_routes');


/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function (ebusa, app, auth, database) {

    app.get('/api/ebusa/example/anyone', function (req, res, next) {

        var logger = logModule.getLogger();
        logger.debug('debug');
        logger.trace('not shown');
        logger.error('error');
        logger.warn('warn');
        res.send('Anyone can access this');
    });

    app.get('/api/ebusa/example/auth', auth.requiresLogin, function (req, res, next) {
        res.send('Only authenticated users can access this');
    });

    app.get('/api/ebusa/example/admin', auth.requiresAdmin, function (req, res, next) {
        res.send('Only users with Admin role can access this');
    });

    app.get('/api/ebusa/example/render', function (req, res, next) {
        ebusa.render('index', {
            package: 'ebusa'
        }, function (err, html) {
            //Rendering a view from the Package server/views
            res.send(html);
        });
    });

    //Videos
    var videoCtrl = require('../controllers/videoController.js')(ebusa);
    var multipartMiddleware = require('connect-multiparty')(ebusa);

    // authorization helpers
    var hasAuthorization = function (req, res, next) {
        if (!req.user.isAdmin && !req.video.user._id.equals(req.user._id)) {
            return res.status(401).send('User is not authorized');
        }
        next();
    };

    app.route('/api/videos')
        .get(auth.isMongoId, auth.requiresLogin, videoCtrl.all)
        .post(auth.isMongoId, auth.requiresLogin, videoCtrl.create, videoCtrl.show);

    app.route('/api/videos/daterange')
        .get(auth.isMongoId, auth.requiresLogin, videoCtrl.getAllBetweenGivenDates);

    app.route('/api/videos/mine')
        .get(auth.isMongoId, auth.requiresLogin, videoCtrl.mine);

    app.route('/api/videos/mother/:motherId')
        .get(auth.isMongoId, auth.requiresLogin, hasAuthorization, videoCtrl.motherVideos);

    app.route('/api/videos/:videoId')
        .get(auth.isMongoId, auth.requiresLogin, videoCtrl.show)
        .delete(auth.isMongoId, auth.requiresLogin, hasAuthorization, videoCtrl.destroy);

    app.route('/api/videos/videofile/:videoId')
        .post(auth.isMongoId, auth.requiresLogin, multipartMiddleware, videoCtrl.addVideoFile);
    
    app.route('/api/videofile/:videoFileId')
        .get(auth.isMongoId, auth.requiresLogin, videoCtrl.videoFile);

    app.param('videoId', videoCtrl.video);
};
