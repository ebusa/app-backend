'use strict';

var log4js = require('log4js');

var PATTERN_CONSOLE = '%[[%d{DATE}] [%p]\t[%x{ln}]\t%m%]';
var PATTERN_FILE = '[%d{DATE}] [%p]\t[%x{ln}]\t%m';

module.exports = function (module) {

    log4js.configure({
        appenders: [
            {
                type: 'dateFile',
                filename: 'logs/ebusa.log',
                pattern: '_yyyy_MM_dd',
                category: ['app'],
                alwaysIncludePattern: false,
                maxLogSize: 8000,
                backups: 5,
                layout: {
                    type: 'pattern',
                    pattern: PATTERN_FILE,
                    tokens: {
                        ln: function () {
                            return module;
                        }
                    }
                }
            },
            {
                type: 'console',
                layout: {
                    type: 'pattern',
                    pattern: PATTERN_CONSOLE,
                    tokens: {
                        ln: function () {
                            return module;
                        }
                    }
                }
            }
        ],
        replaceConsole: true,
        levels: {
            '[all]': 'DEBUG'
        }
    });

    return {
        getLogger: function () {
            var logger = log4js.getLogger('app');
            return logger;
        }
    };
};