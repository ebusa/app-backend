'use strict';

var Video, Q, User, conn, fs, mongoose, request, should;

should = require('should');

mongoose = require('mongoose');

User = mongoose.model('User');

Video = mongoose.model('Video');

request = require('superagent');

Q = require('q');

conn = mongoose.connection;

fs = require("fs");

describe('Video Controllers', function () {
    var createVideos, createSampleVideo, login, logout, user;
    user = null;

    /**
     * Helper function to perform form-based login
     * (our app does not support basic authentication)
     * @return a promise containing the JWT token
     */
    login = function (agent) {
        var deferred;
        deferred = Q.defer();
        agent.post('http://localhost:3001/api/login').send({
            email: user.email,
            password: user.password
        }).end(function (err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            deferred.resolve(res.body.token);
        });
        return deferred.promise;
    };

    /**
     * Helper function to perform logout
     * @return a promise
     */
    logout = function (agent, token) {
        var deferred;
        deferred = Q.defer();
        agent.get('http://localhost:3001/api/logout').set("Authorization", "Bearer " + token).end(function (err, res) {
            should.not.exist(err);
            res.status.should.be.equal(200);
            deferred.resolve(res);
        });
        return deferred.promise;
    };

    /**
     * Initialize the entire Spec
     * This is done only once and it will create a test user
     */
    before(function (done) {
        user = new User({
            name: 'John Doe',
            email: 'jd@test.com',
            username: 'jd',
            password: 'topsecret'
        });
        user.save(function () {
            done();
        });
    });

    /**
     * Helper method to create and store one single Video
     * @return a method that returns a promise
     */

    createSampleVideo = function (nr, usr) {
        if (usr == null) usr = user;

        return function () {
            var d = Q.defer();

            var video = new Video({
                information: "Information " + nr,
                creator: usr
            });
            video.save(function (err, video) {
                if (err) {
                    d.reject(err);
                }
                Q.delay(10).then(function () {
                    d.resolve(video);
                });

            });
            return d.promise;
        };
    };

    /**
     * A helper method that creates a number of Videos sequentially
     * @return a promise that is resolved once all videos are created
     */
    createVideos = function (nr) {
        var i, results;
        return (function () {
            results = [];
            for (var i = 1; 1 <= nr ? i <= nr : i >= nr; 1 <= nr ? i++ : i--) {
                results.push(i);
            }
            return results;
        }).apply(this).reduce((function (p, n) {
            return p.then(createSampleVideo(n));
        }), Q.delay(10));
    };

    describe("Video create", function () {
        it("should be possible to create a new Video", function (done) {
            var agent;
            agent = request.agent();
            login(agent).then(function (token) {
                var deferred = Q.defer();
                agent
                    .post('http://localhost:3001/api/videos')
                    .set("Authorization", "Bearer " + token).send({
                    information: "Information"
                }).end(function (err, res) {
                    var video;
                    should.not.exist(err);
                    res.status.should.be.equal(200);
                    video = res.body;
                    video.information.should.be.equal("Information");
                    video.creator.name.should.be.equal("John Doe");
                    video.creator.username.should.be.equal("jd");
                    deferred.resolve(token);
                });
                return deferred.promise;
            }).then(function (token) {
                logout(agent, token);
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe("add Video", function () {
        it("should allow to add a video file to an existing Video", function (done) {
            var JWTToken = {};
            this.timeout(10000);
            var agent = request.agent();

            login(agent).then(function (token) {
                JWTToken = token;
                return createSampleVideo(1)();
            }).then(function (video) {
                var d = Q.defer();
                agent.post("http://localhost:3001/api/videos/videofile/" + video._id)
                    .set("Authorization", "Bearer " + JWTToken)
                    .attach('file', fs.createReadStream('packages/custom/ebusa/server/tests/test_files/test_video.mp4'))
                    .end(function (err, res) {
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        var imgData = res.body;
                        imgData.filename.should.be.equal('test_video.mp4');
                        imgData.contentType.should.be.equal('video/mp4');
                        imgData.metadata.video.should.be.equal(video._id.toString());
                        imgData.metadata.creator.should.be.equal(user._id.toString());
                        return d.resolve(video);
                    });
                return d.promise;
            }).then(function (video) {
                should.exist(video.videoFile);
                var q = Q;
                return q('ok');
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
        var remove = function (collectionName) {
            var d = Q.defer();
            conn.db.collection(collectionName).remove(function (err, nr) {
                if (err) {
                    d.reject(err);
                } else {
                    d.resolve(nr);
                }
            });
            return d.promise;
        };
        afterEach(function (done) {
            return remove('fs.files').then(function () {
                remove('fs.chunks');
            }).then(function () {
                done();
            });
        });
    });

    describe("Read a single Video", function () {
        it("should be possible to load a single Video by its id", function (done) {
            var JWTToken = {};
            var agent = request.agent();
            login(agent)
                .then(function (token) {
                    JWTToken = token;
                    return createSampleVideo(1)();
                }).then(function (video) {
                var deferred = Q.defer();
                agent.get("http://localhost:3001/api/videos/" + video._id)
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        var video = res.body;
                        video.information.should.be.equal("Information 1");
                        should.exist(video._id);
                        should.exist(video.creator);
                        video.creator.name.should.be.equal("John Doe");
                        video.creator.username.should.be.equal("jd");
                        deferred.resolve(video);
                    });
                return deferred.promise;
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe("My Videos", function () {
        it("should only load those videos created by the current user", function (done) {
            var agent = request.agent();
            var jesseJames = null;
            var JWTToken = {};

            var createJesse = function () {
                var d = Q.defer();
                var jesse = new User({
                    name: 'Jesse James',
                    email: 'jj@test.com',
                    username: 'jj',
                    password: 'topsecret'
                });
                jesse.save(function (err, jesse) {
                    if (err) {
                        d.reject(err);
                    }
                    d.resolve(jesse);
                });
                return d.promise;
            };

            createSampleVideo(1)()
                .then(createJesse)
                .then(function (jesse) {
                    jesseJames = jesse;
                    return createSampleVideo(1, jesse)();
                }).then(function () {
                return login(agent, jesseJames);
            }).then(function (token) {
                var d = Q.defer();
                agent.get('http://localhost:3001/api/videos/mine')
                    .set("Authorization", "Bearer " + token)
                    .end(function (err, res) {
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        var videos = res.body;
                        videos.should.have.lengthOf(1);
                        videos[0].information.should.be.equal("Information 1");
                        d.resolve(res);
                    });
                return d.promise;
            }).then(function () {
                jesseJames.remove();
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe("Video list all ", function () {
        it("should list the latest ten entries", function (done) {
            var JWTToken = {};
            var agent = request.agent();

            login(agent).then(function (token) {
                JWTToken = token;
                return createVideos(12);
            }).then(function () {
                var deferred = Q.defer();
                agent.get('http://localhost:3001/api/videos')
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        var index, i;
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        var videos = res.body;
                        videos.should.have.a.lengthOf(10);
                        for (index = i = 0; i <= 9; index = ++i) {
                            videos[index].information.should.be.equal("Information " + (12 - index));
                        }
                        deferred.resolve();
                    });
                return deferred.promise;
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("should support pagination", function (done) {
            var JWTToken = {};
            var agent = request.agent();
            var getPage = function (page, size, numberOfVideos, resultLength) {
                var deferred = Q.defer();
                agent.get("http://localhost:3001/api/videos?size=" + size + "&page=" + page)
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        var i, index, videos, ref;
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        videos = res.body;
                        videos.should.have.a.lengthOf(resultLength);
                        for (index = i = 0, ref = resultLength - 1; 0 <= ref ? i <= ref : i >= ref; index = 0 <= ref ? ++i : --i) {
                            videos[index].information.should.be.equal("Information " + (numberOfVideos - index));
                        }
                        deferred.resolve();
                    });
                return deferred.promise;
            };

            login(agent).then(function (token) {
                JWTToken = token;
                return createVideos(12);
            }).then(function () {
                return getPage(0, 8, 12, 8);
            }).then(function () {
                return getPage(1, 8, 4, 4);
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe("Video list all between given dates ", function () {
        it("should list the latest ten entries", function (done) {
            var JWTToken = {};
            var agent = request.agent();

            login(agent).then(function (token) {
                JWTToken = token;
                return createVideos(12);
            }).then(function () {
                var deferred = Q.defer();

                var startDate = new Date();
                startDate.setDate(startDate.getDate());
                var enddate = new Date();
                enddate.setDate(enddate.getDate());

                agent.get('http://localhost:3001/api/videos/daterange?startdate=' + startDate.toDateString()
                        + "&enddate=" + enddate.toDateString())
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        var index, i;
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        var videos = res.body;
                        videos.should.have.a.lengthOf(10);
                        for (index = i = 0; i <= 9; index = ++i) {
                            videos[index].information.should.be.equal("Information " + (12 - index));
                        }
                        deferred.resolve();
                    });
                return deferred.promise;
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it("should support pagination", function (done) {
            var JWTToken = {};
            var agent = request.agent();
            var getPage = function (page, size, numberOfVideos, resultLength) {
                var deferred = Q.defer();

                var startDate = new Date();
                startDate.setDate(startDate.getDate());
                var enddate = new Date();
                enddate.setDate(enddate.getDate());

                agent.get("http://localhost:3001/api/videos?size=" + size + "&page=" + page +
                        "&startdate=" + startDate.toDateString() + "&enddate=" + enddate.toDateString())
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        var i, index, videos, ref;
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        videos = res.body;
                        videos.should.have.a.lengthOf(resultLength);
                        for (index = i = 0, ref = resultLength - 1; 0 <= ref ? i <= ref : i >= ref; index = 0 <= ref ? ++i : --i) {
                            videos[index].information.should.be.equal("Information " + (numberOfVideos - index));
                        }
                        deferred.resolve();
                    });
                return deferred.promise;
            };

            login(agent).then(function (token) {
                JWTToken = token;
                return createVideos(12);
            }).then(function () {
                return getPage(0, 8, 12, 8);
            }).then(function () {
                return getPage(1, 8, 4, 4);
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe("Delete a single video", function () {
        it("should be possible to delete a single Video by its id", function (done) {
            var JWTToken = {};
            var agent = request.agent();
            login(agent)
                .then(function (token) {
                    JWTToken = token;
                    return createSampleVideo(1)();
                }).then(function (video) {
                var deferred = Q.defer();

                agent.delete("http://localhost:3001/api/videos/" + video._id)
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        var video = res.body;
                        video.information.should.be.equal("Information 1");
                        should.exist(video._id);
                        should.exist(video.creator);
                        video.creator.name.should.be.equal("John Doe");
                        video.creator.username.should.be.equal("jd");
                        deferred.resolve(video);
                    });

                return deferred.promise;
            }).then(function (video) {
                var deferred = Q.defer();
                Video.findOne({
                    _id: video._id
                }, function (err, result) {
                    should.not.exist(result);
                    deferred.resolve();
                });
                return deferred.promise;
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe("Video list all of mother", function () {
        it("should list the latest ten entries of one mother", function (done) {
            var JWTToken = {};
            var agent = request.agent();

            login(agent).then(function (token) {
                JWTToken = token;
                return createVideos(12);
            }).then(function () {
                var deferred = Q.defer();

                agent.get('http://localhost:3001/api/videos/mother/' + user._id)
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        var index, i;
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        var videos = res.body;
                        videos.should.have.a.lengthOf(10);
                        for (index = i = 0; i <= 9; index = ++i) {
                            videos[index].information.should.be.equal("Information " + (12 - index));
                        }
                        deferred.resolve();
                    });
                return deferred.promise;
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
        it("should support pagination", function (done) {
            var JWTToken = {};
            var agent = request.agent();
            var getPage = function (page, size, numberOfVideos, resultLength) {
                var deferred = Q.defer();
                agent.get("http://localhost:3001/api/videos/mother/" + user._id + "?size=" + size + "&page=" + page)
                    .set("Authorization", "Bearer " + JWTToken)
                    .end(function (err, res) {
                        var i, index, videos, ref;
                        should.not.exist(err);
                        res.status.should.be.equal(200);
                        videos = res.body;
                        videos.should.have.a.lengthOf(resultLength);
                        for (index = i = 0, ref = resultLength - 1; 0 <= ref ? i <= ref : i >= ref; index = 0 <= ref ? ++i : --i) {
                            videos[index].information.should.be.equal("Information " + (numberOfVideos - index));
                        }
                        deferred.resolve();
                    });
                return deferred.promise;
            };

            login(agent).then(function (token) {
                JWTToken = token;
                return createVideos(12);
            }).then(function () {
                return getPage(0, 8, 12, 8);
            }).then(function () {
                return getPage(1, 8, 4, 4);
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    afterEach(function (done) {
        Video.remove({}).exec().then(function () {
            done();
        });
    });

    /**
     * Cleaning up after all tests have been performed
     * Here we remove our test user from the database
     */
    after(function (done) {
        User.remove({}).exec().then(function () {
            done();
        });
    });
});

